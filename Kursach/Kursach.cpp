﻿#include <iostream>
#include <stdlib.h> 
#include <stdio.h>
#include <conio.h>
#include <Windows.h>
#include <exception>
#include <excpt.h>
#include <string.h>     /*для работы с консолью, строками*/


//----------------------------------------------Множества----------------------------------------------------------
enum ConsoleColor // Цвета для установки консолей
{
	Black = 0,
	Blue = 1,
	Green = 2,
	Cyan = 3,
	Red = 4,
	Magenta = 5,
	Brown = 6,
	LightGray = 7,
	DarkGray = 8,
	LightBlue = 9,
	LightGreen = 10,
	LightCyan = 11,
	LightRed = 12,
	LightMagenta = 13,
	Yellow = 14,
	White = 15
};

enum MainMenu { // Множество пунктов меню
	View,
	Add,
	Del,
	Edit,
	Sort,
	Find,
	Save,
	Task,
	Exit
};

//----------------------------------------------константы----------------------------------------------------------
const int COUNT_MAIN_MENU_ITEMS = 9;				// количество меню
const int SURNAME_LENGTH = 20;			//Длина фамилии
const int COUNT_ROW_PAGE = 29;			// количество элементов на странице
const int COUNT_SYMBOL_ROWS = 120;     // Количество символов в строке
const int COUNT_PLUNT_MENU = 7;			// количество элементов в заголовках таблицы завода
const int FILE_NAME_LENGTH = 255;			// Длина файла
const std::string NAME_FILE_DIRECTORY = "C:\\temp\\";// папка где хранятся файлы
const std::string CLEAR_ROW = "                                                                                                                        ";// пустая строка для очистки

const std::string** MAIN_MENU_ITEM = new const std::string * [COUNT_MAIN_MENU_ITEMS] {
	new std::string("Просмотр"),
		new std::string("Добавить"),
		new std::string("Удалить"),
		new std::string("Исправить"),
		new std::string("Сортировать"),
		new std::string("Найти"),
		new std::string("Сохранить"),
		new std::string("Ведомость"),
		new std::string("Выход")
};

const std::string** PLUNT_MENU_ITEM = new const std::string * [COUNT_PLUNT_MENU] {
	new std::string("Номер записи"),
		new std::string("Номер завода"),
		new std::string("Номер филиала"),
		new std::string("Фамилия"),
		new std::string("Было"),
		new std::string("Получено"),
		new std::string("Отдано")
};

//------------------------------------типы и глобальные переменные-------------------------------------
struct plunt {							// структура завода
	int pluntNumber;					// Номер завода
	int filialNumber;					// Номер филиала
	char surname[SURNAME_LENGTH];		// Фамилия
	double startPeriodMoney;				// Количество ценностей на начало периода
	double receivedMoney;					// Получено материальных ценностей
	double droppedMoney;					// Упущено материальных ценностей
};

struct elist			// элемент двунаправелнного списка
{
	plunt data;			// Данные о заводе
	elist* left;		// Указатель на элемент слева
	elist* right;		// указатель на элемент справа
};


//-----------------------------------------прототипы функций-------------------------------------------------
void add(elist*& beg, elist*& fin, plunt telefon);					//добавление элемента в список
void error(const std::string message);											//вывод сообщения ошибки
int readfile(elist*& beg, elist*& fin, char* fileName);					//ввод базы из файла
int showbase(elist* beg);								//вывод базы на экран
void gotoxy(int x, int y);							// Установить курсор в позицию
void drawMenu(int number, const std::string** menuItem, int countMenuItem); // Отрисовка меню
MainMenu getMenuItem(const std::string** menuItem, int numberMenu); //получения меню 
void SetColor(ConsoleColor text, ConsoleColor background); //установка цвета
void clear(); // очистка
void addNewPlunt(elist*& beg, elist*& fin); //добавление нового завода
void inputNewPluntNode(plunt& newPlunt); // Ввод нового элемента таблицы
void checkInput(int digit); // проверка валидности данных
elist* findByFilial(int numberFilial, elist*& beg); // поиск филиала
void editElementList(elist* editEleemnt, elist*& beg); // редактирование элемента списка
void printElementList(elist* printElement); // печать элемента
void delElementList(elist*& beg, elist*& fin, elist* delElement); // удаление элемента
void sortElementsList(elist*& beg, elist*& fin); // сортировка элементов списка
void saveFile(char* fileName, elist*& beg); // сохранение в файл
void calculationTask(elist*& beg, elist*& fin); // основное задание

//========================Главная функция============================
int main()
{
	setlocale(LC_ALL, "Russian"); //Устанавливаем русский язве
	SetConsoleCP(866); // Устновка кодировки консоли
	SetConsoleTitle(L"Мой курсач. Это лучший курсач в мире."); // Заголовок окна

	elist* beg, * fin; // начальные элементы списка
	beg = fin = nullptr;//обнуляем

	SetColor(ConsoleColor::Black, ConsoleColor::White);//цвет текст и фона
	system("cls"); // очищаем консоль
	elist* editElement = new elist;
	int numberMenuItem = 0;// начальный элемент меню
	char* fileName = new char[FILE_NAME_LENGTH];// переменная для имени файла
	bool isOpenFile = false; // если файл открыт
	/*
	суть программы в том, что пока файл не открыт, ты ничего сделать не можешь
	можно открыть чистый файл или уже существующий
	после того как мы открыли файл, мы пользуемся кнопкой просмотр для просмотра текущих значений
	как только мы сохраняем файл у нас пропадает таблица и файл надо открывать заново, в этом вся логика
	при желание её можно легко изменить
	!!!!!!!!Ключевое поле это филиал, так как у одного завода может быть несколько филиалов и у 2 заводах  не может быть одинаковых филиалов
	поэтому значения завода повторяются, а значения филиала нет. Значения филиала уникальны
	*/
	while (1) {// бесконечный цикл
		drawMenu(numberMenuItem, MAIN_MENU_ITEM, COUNT_MAIN_MENU_ITEMS);//рисуем меню

		int key = _getch();// считываем клавишу
		if (key == 244) {//если расширенный код
			key = _getch();
		}

		switch (key)
		{
			// клавиша вправо
		case 77:
			numberMenuItem++; // увеличиваем номер меню

			break;

			// клавиша влево
		case 75:
			numberMenuItem--; // уменьшаем номер меню

			break;

		case 13:
			switch (getMenuItem(MAIN_MENU_ITEM, numberMenuItem))// если нажата enter то переходим в выбранный пункт меню
			{

			case MainMenu::View: // просмотр

				clear();// очищаем
				gotoxy(0, 2); // устанавливаем курсор на начало 2 строки

				if (!isOpenFile) { // Если файл не открыт
					printf_s("Введите имя файла: ");
					SetConsoleCP(1251); //кодировка консоли для поддердки русских символов
					fgets(fileName, FILE_NAME_LENGTH, stdin); // получаем имя фалйа
					SetConsoleCP(866); // возврашаем кодировку

					beg = fin = 0; //обнулить указатели на начало и конец списк
					if (readfile(beg, fin, fileName) != 1) { // если успешно файл прочитан
						isOpenFile = true;//ставим флаг открытого флага
					}
				}
				else {
					printf_s("Введите имя файла: "); //печатем имя фалйа
					printf_s(fileName);
				}

				if (isOpenFile)
					showbase(beg);//если файл открыт показываем таблицу

				break;

			case MainMenu::Add: // добавление
				if (isOpenFile) {// если файл открыт
					addNewPlunt(beg, fin); // вызываем функцию
					printf("Запись успешно добавлена.");
				}
				else {
					error("Для добавления создайте таблицу."); //выводим ошибку
				}

				break;

			case MainMenu::Del: // удление
				if (isOpenFile) { //если файл открыт
					try { // try cath это обработка исключений, если возникла исключительная ситуация, то программа прерывается и переходит в блок catch
						clear();
						int numberFilial;
						gotoxy(0, 2);
						printf("Удаление записи.\n");
						printf("Введите номер филиала: ");
						checkInput(scanf_s("%d", &numberFilial)); //получаем номер филила

						elist* delElement = findByFilial(numberFilial, beg); // ищем филиал

						if (delElement == nullptr) {
							error("Такого филиала нет");
						}
						else {
							delElementList(beg, fin, delElement);// удалаяем филиал
							printf("Запись успешно удалена");
						}
					}
					catch (int ex) {
						fseek(stdin, 0, SEEK_END); // Очистка буфера stdin - stdin это входной поток данных
					}
				}
				else {
					error("Таблица не создана. Операция невозможна.");
				}


				break;

			case MainMenu::Edit:// редактирование
				if (isOpenFile) {
					try {
						int numberFilial;
						clear();
						gotoxy(0, 2);
						printf("Редактирование записи\n");
						printf("Введите номер филиала: ");
						checkInput(scanf_s("%d", &numberFilial)); //вводим филиал

						editElement = findByFilial(numberFilial, beg); // ищем его

						if (editElement == nullptr) { //если его нет
							error("Такого филиала нет");
						}
						else {
							editElementList(editElement, beg);
							printf("Запись успешно изменена");
						}
					}
					catch (int ex) {
						fseek(stdin, 0, SEEK_END); // Очистка буфера stdin

					}
				}
				else {
					error("Таблица не создана. Операция невозможна.");
				}

				break;

			case MainMenu::Sort: // сортировка
				if (isOpenFile) {
					clear();// очищаем
					sortElementsList(beg, fin); // сортируем таблицу по ключевому полю
					gotoxy(0, 2);
					printf("Отсортированная таблица по филиалам");
					showbase(beg); // показываем таблицу
				}
				else {
					error("Таблица не создана. Операция невозможна.");
				}

				break;

			case MainMenu::Find: // поиск записи
				if (isOpenFile) {
					try {
						clear();
						int numberFilial;
						gotoxy(0, 2);
						printf("Поиск записи.\n");
						printf("Введите номер филиала: ");
						checkInput(scanf_s("%d", &numberFilial)); //вводим филиал

						elist* findElement = findByFilial(numberFilial, beg);
						// если нашли, то выводим его, если нет, то ошибка
						if (findElement == nullptr) {
							error("Такого филиала нет");
						}
						else {
							printf("Запись успешно найдена.\n");
							printElementList(findElement);
						}
					}
					catch (int ex) {
						fseek(stdin, 0, SEEK_END); // Очистка буфера stdin
					}
				}
				else {
					error("Таблица не создана. Операция невозможна.");
				}
				break;

			case MainMenu::Save: // сохрранени по аналогии с чтением
				if (isOpenFile) {
					clear();
					gotoxy(0, 2);
					printf("Сохранение таблицы \n");
					printf("Введите имя файла: ");
					fseek(stdin, 0, SEEK_END); // Очистка буфера scanf
					SetConsoleCP(1251);
					fgets(fileName, FILE_NAME_LENGTH, stdin);
					SetConsoleCP(866);

					saveFile(fileName, beg);

					printf("Файл успешно сохранён.");
					isOpenFile = false; // закрываем текущий файл и удаляем страницу к херам
				}
				else {
					error("Таблица не создана. Операция невозможна.");
				}

				break;

			case MainMenu::Task: //Задание
				if (isOpenFile) {
					clear();
					gotoxy(0, 2);
					calculationTask(beg, fin);//вызываем функцию обработки данных
				}
				else {
					error("Таблица не создана. Операция невозможна.");
				}

				break;

			case MainMenu::Exit:
				system("cls");//очищаем консоль
				printf("Спасибо за то, что пользуетесь нашей программой :)\n");//выводим надпись

				return 0;
				break;

			default:
				break;
			}

			break;

		default:
			break;
		}

		if (numberMenuItem < 0) {//если номер меню меньше 0
			numberMenuItem = COUNT_MAIN_MENU_ITEMS - 1;//номер меню равен последнему элементу меню
		}
		else {
			if (numberMenuItem == COUNT_MAIN_MENU_ITEMS) //иначе если он равен количеству меню. Немного поясню. Массивы нумеруются с 0, значит в массиве из 7 элементов последний жемент имеет индекс 6, 
				// то бишь если равен 7, то значит он за границей массива и ставим ему начало меню
			{
				numberMenuItem = 0; // то номер меню равен нулю
			}
		}

	}

	system("pause");  // Ожидаем нажатие клавиши
}

//--------------------------------------Устнановка цвета текста и фона-------------------------
void SetColor(ConsoleColor text, ConsoleColor background)
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE); // получаем потток вывода
	SetConsoleTextAttribute(hStdOut, (WORD)((background << 4) | text) // тут происходит сущая магия // берём число background делаем побитовый сдвиг влево на 4 // Получаем число 
		// потом происходит поразрядное или между полученным числом и числом текста, после преобразуем в тип word. ПРосто магия
	);
}

//--------------------------------------Очистка всего того, что под меню-------------------------
void clear() {
	gotoxy(0, 2); // Начиная со второй строки консоли // Строки нумеруются с нуля
	for (int i = 2; i <= COUNT_ROW_PAGE; i++) { // начиная с 2по количество строк на странице
		printf(CLEAR_ROW.c_str()); // Выводим длинную пустую строку
	}
}

//--------------------------------------Ввод нового элемента таблицы-------------------------
void addNewPlunt(elist*& beg, elist*& fin) {
	clear(); // Очищаем консоль
	plunt newPlunt; //новый элемент
	gotoxy(0, 2); // ставим указатель на начало 2 строки

	try {
		printf("Добавление новой записи\n"); // выводим
		inputNewPluntNode(newPlunt); //вызываем функцию ввода данных

		if (findByFilial(newPlunt.filialNumber, beg) == nullptr) { //проверяем существование такого элемента в списке
			add(beg, fin, newPlunt); // вызываем функцию добавления элемента в список
		}
		else {
			error("Запись с таким филиалом уже существует"); // вызываем функцию ошибки
			throw  1; // выкидываем код в блок catch
		}
	}
	catch (int i) {// i- номер ошибки
		fseek(stdin, 0, SEEK_END); // Очистка буфера scanf, от 0, до указатель на последний символ
	}


}

//---------------------------------------Ввод новых данных------------------------
void inputNewPluntNode(plunt& newPlunt)
{
	printf("Введините номер завода цифрами: ");
	checkInput(scanf_s("%d", &newPlunt.pluntNumber)); // проверка данных

	printf("Введините номер филиала цифрами: ");
	checkInput(scanf_s("%d", &newPlunt.filialNumber)); // проверка данных

	printf("Введините фамилию(20 символов): ");
	SetConsoleCP(1251); // Устанавливаем кодировку консоли на чтение русского языка
	scanf_s("%s", newPlunt.surname, SURNAME_LENGTH); // считываем определённое количество символов
	SetConsoleCP(866); // возращаем исходную кодировку

	printf("Введините количество материальных ценносте на начало периода(формат 00,00): ");
	checkInput(scanf_s("%lf", &newPlunt.startPeriodMoney)); // проверка данных

	printf("Введините количество полученных материальных ценностей(формат 00,00): ");
	checkInput(scanf_s("%lf", &newPlunt.receivedMoney)); // проверка данных

	printf("Введините количество потерянных материальных ценностей(формат 00,00): ");
	checkInput(scanf_s("%lf", &newPlunt.droppedMoney)); // проверка данных

	/*
	Очень важная информация по scanf_s printf_s и подобным си функциям. Будут фигурировать такие знаки как. Напишу один раз потом писать не буду
	%d - ожидается число
	%s - ожидается строка
	%ld - ожидается тип double
	\t - это табуляция - несколько пробелов подряд
	\n - символ новой строки
	* - любое количество символов
	*/
}

//---------------------------------------Проверка данных------------------------
void checkInput(int digit) {
	if (digit == 0) { // Если равняесят 0, то во время считывания произошла ошибка
		error("Ошибка ввода"); // выводим ошибку
		throw 1; // кидаем исключение
	}
}

//---------------------------------------Поиск по филиалу------------------------
elist* findByFilial(int numberFilial, elist*& beg)
{
	elist* p = beg; // устанавливаем р в начало списка
	while (p != nullptr && p->data.filialNumber != numberFilial) {// пока не дойдём до окончания списка и нужного элемента
		p = p->right; // переход к следующему элементу
	}

	return p; // возращаем р, если = null, то элемент не найден
}

//-------------------------------------------Редактирование элемента-------------------------------------
void editElementList(elist* editEleemnt, elist*& beg) {
	printElementList(editEleemnt); // распечатываем элемент
	printf("Введите новые значения\n");

	plunt newPlunt; // новый элемент
	inputNewPluntNode(newPlunt); // вводим новый элемент

	int numberFilial = editEleemnt->data.filialNumber; // запоминаем фелиал элемента до редактирования

	editEleemnt->data.filialNumber = INT_MIN; // присваиваем филиалу текущегго элемента минимальное значение int, чтобы проверить, что добавляем с филиалом, которого нет в спсике

	if (findByFilial(newPlunt.filialNumber, beg) == nullptr) { // если такой фелил не найден
		editEleemnt->data = newPlunt; // заменяем элемент на списке на новый
	}
	else {
		editEleemnt->data.filialNumber = numberFilial; // возращаем новый филиал
		error("Такой филиал уже существует"); // выводим ошибку
		throw 1; // кидаем исключение
	}
}

//--------------------------------------Распечатать элемент-------------------------
void printElementList(elist* printElement) {
	// Здесб просто распечатка, про символы печати я рассказывал выше
	printf_s("%s%d\n", "Номер завода: ", printElement->data.pluntNumber);
	printf_s("%s%d\n", "Номер филиал: ", printElement->data.filialNumber);
	printf_s("%s%s", "Фамилия: ", printElement->data.surname);
	printf_s("%s%lf\n", "Начальное: ", printElement->data.startPeriodMoney);
	printf_s("%s%lf\n", "Получено: ", printElement->data.receivedMoney);
	printf_s("%s%lf\n", "Ушло: ", printElement->data.droppedMoney);
}

//--------------------------------------Удаление элемента-------------------------
void delElementList(elist*& beg, elist*& fin, elist* delElement) {
	if (delElement == beg) { // если элемент равен первому элементу
		beg = beg->right; // начало списка перемещаем на элемент справа
		beg->left = nullptr; // у начала списка элемент слева становится равен нал
		delete delElement; // удаляем его

		return; // выходим из метода
	}

	if (delElement == fin) { // если равен последнему элементу списка
		fin = fin->left; // перемещаем конец списка на элемент влево
		fin->right = nullptr; // элемент справа равен 0
		delete delElement; // удаляем элемент

		return; //выходим из функции
	}

	if (delElement != nullptr) { // если элемент внутри и не равен нал
		delElement->left->right = delElement->right; // правый элемент текущего элемента слева, указывает на правый элемент текущего элемента
		delElement->right->left = delElement->left; // левый элемент текущего элемента справа, указывает на левый элемент текущего элемента 
		delete delElement; // удаляем, освобождаем память, как нравится, так и пишем
	}

}

//--------------------------------------Сортировка элемнтов списка-------------------------
void sortElementsList(elist*& beg, elist*& fin) {
	elist* p = beg; // p Присваем начало списка
	elist* begin = nullptr, * finish = nullptr; // ОБъявляем начало и конец нового списка

	while (p != nullptr) {// Пока не достигнем окончания списка.
		if (begin == nullptr) { // Если новой список пуст
			begin = finish = new elist(); // Началу и концу списка присваем новый элемент
			begin->left = nullptr; // устанавливаем значение элемента слева нал
			finish->right = nullptr; // устанавливаем значение элемента Справа над
			begin->data = p->data; // Начальному элементу списка присваиваем данные текущего жлемента
		}
		else { // Если список не пустой
			elist* q = begin; // берём значение первого элемента нового списка
			while (q != nullptr && q->data.filialNumber <= p->data.filialNumber) { // проходим вперёд по списку пока номер филиала в новом списке, <= номеру филиала в текущем элементе старого списка
				q = q->right;
			}

			elist* newelist = new elist(); // создаём новый элемент
			newelist->data = p->data; // Присваиваем данные текущего элемента старого списка.

			if (q == begin) { // если это первый элемент
				newelist->left = nullptr; // значение слева нал
				newelist->right = begin; // значение справа будет, текущее начало нового списка
				begin->left = newelist; // значение слева от текущего начало списка равняется новому элементу
				begin = newelist; // перемещаем начало нового списка на новый элемент
			}
			else {
				if (q == nullptr) { // Если равняется нулл, значит это конец списка
					newelist->left = finish; // значит слева от нового элемента конец нового списка
					newelist->right = nullptr; // справа нал
					finish->right = newelist; // справа от текущего конца нового списка новый элемент
					finish = newelist; // перемещаем конец списка на новый элемент
				}
				else { // иначе это в между начало и концом, вставляем перед найденным элементом
					newelist->left = q->left; // элемент слева нового элемента. равняется элменту слева найденного элемента
					newelist->right = q; // правый элемент нового элемента равняется найденному элементу
					q->left->right = newelist; // правый элемент, левого элемента от найденного равняется новому элементу
					q->left = newelist; // левый элемент найденного элемента равняется новому элементу
					// например надо вставить 4 в 3 4 5  тогда найденный элемент будет 5, а итого будет 3 4 4 5
				}
			}


		}
		p = p->right; // переходим к следующему элементу
	}

	beg = begin; // начало исходного списка, равняется новому списку
	fin = finish; // конец исходного списка, равняется новому списку
}

//--------------------------------------Отрисовываем меню-------------------------
void drawMenu(int numberMenuItem, const std::string** menuItem, int countMenuItem) {
	SetColor(ConsoleColor::Red, ConsoleColor::White); // устнавливаем цвета фона и текста
	for (int j = 0; j < COUNT_SYMBOL_ROWS; j++) { // рисуем красную линию
		gotoxy(j, 1);
		printf("=");
	}
	printf("\n"); // перходим на новую стоку


	for (int i = 0; i < countMenuItem; i++) { // проходим по списку меню
		gotoxy(14 * i, 0); //устанавливаем курсовр в позицию номера текущего элемента * 14, на 0 строке
		SetColor(ConsoleColor::Green, ConsoleColor::White); // фон и текси
		if (i == numberMenuItem) // если равен выбранному меню
		{
			SetColor(ConsoleColor::Green, ConsoleColor::LightGray); // делаем фон текста лего серым
			printf(menuItem[i]->c_str()); // выводим пункт меню
		}
		else
		{
			printf(menuItem[i]->c_str()); // выводим пункт меню
		}
	}

	SetColor(ConsoleColor::Black, ConsoleColor::White); // Возращаем исходный текст
}

//-----------------------------Вывод базы на экран---------------------
int showbase(elist* beg) {
	int i; //счетчик кол-ва строк

	if (!beg) { //если список пустой,
		gotoxy(0, 3);
		printf("Список пустой"); //то вывести сообщение
		return -1; // выйти из функции
	}
	int countPluntRow = 26; // количество вохожных строк

	int numberPage = 1; // номер страницы
	int keyCode = 0; // код клавиши
	elist* p = beg, * lastIndex = nullptr; //указатель на начло списка и последний элемент на странице
	elist* firstNodePage = p; // указатель на первый элемент на станичце
	while (true) {
		int strRow = 3; // номер строки с которой начинается

		SetColor(ConsoleColor::Black, ConsoleColor::White); // устанавливаем текст и цвет

		// выводим поля завод (заголовки столбцов), по аналогии с выводом меню
		for (int i = 0; i < COUNT_PLUNT_MENU; i++) {
			gotoxy(i * 17, strRow);
			printf(PLUNT_MENU_ITEM[i]->c_str());
		}

		// очищаем всё что ниже заголовко столбцов
		for (int i = 1; i < countPluntRow + 1; i++) {
			gotoxy(0, strRow + i);
			printf(CLEAR_ROW.c_str());
		}

		strRow++; // переходим на новую строку
		int numberNode = 1; // номер записи на странице
		while (p != nullptr && numberNode != countPluntRow) // пока не наступит конец цикла или не выведется определённое количество записей
		{
			int leftPosition = 0; // отступ слева
			int koefLeftPosiotn = 17; // коэфциент отступа

			// ниже всё однотипно и понятно
			// устанавливаем курсор
			// numberNode++ увеличивает номер записи на 1
			// печатаем
			// увеличиваем левую позицию на коэф-т
			gotoxy(leftPosition, strRow);
			printf("%d", numberNode++);
			leftPosition += koefLeftPosiotn;

			gotoxy(leftPosition, strRow);
			printf("%d", p->data.pluntNumber);
			leftPosition += koefLeftPosiotn;

			gotoxy(leftPosition, strRow);
			printf("%d", p->data.filialNumber);
			leftPosition += koefLeftPosiotn;

			gotoxy(leftPosition, strRow);
			printf(p->data.surname);
			leftPosition += koefLeftPosiotn;

			gotoxy(leftPosition, strRow);
			printf("%lf", p->data.startPeriodMoney);
			leftPosition += koefLeftPosiotn;

			gotoxy(leftPosition, strRow);
			printf("%lf", p->data.receivedMoney);
			leftPosition += koefLeftPosiotn;

			gotoxy(leftPosition, strRow);
			printf("%lf", p->data.droppedMoney);
			leftPosition += koefLeftPosiotn;

			strRow++;
			// если конец, то запоминаем последний элемент
			if (p->right == nullptr)
				lastIndex = p;

			p = p->right; // переход к следующему элменту

			//выводим номер страницы
			gotoxy(0, COUNT_ROW_PAGE);
			printf("Страница: %d", numberPage);

		}
		// если был конец, то возращаем p последний элемнт
		if (lastIndex != nullptr) {
			p = lastIndex;

			lastIndex = nullptr;
		}


		do {
			keyCode = _getch();// код клавиши
			if (keyCode == 224) { // если расширенный код считываем ещё раз
				keyCode = _getch();
			}
		} while (keyCode != 27 // пока не нажата нужная клавиша, мы ждём клавиши, чтобы всё не перерисовывать
			&& keyCode != 13
			&& keyCode != 80
			&& keyCode != 81
			&& keyCode != 72
			&& keyCode != 73);

		switch (keyCode) // свитч по номеру клавиши
		{
		case 27: //нажата Esc
			return 0; //выход из просмотра

		case 13: //если нажаты Enter||Down||PgDn
		case 80:
		case 81:
			if (numberNode == countPluntRow) { // если на странице нужное количество записей
				if (p == nullptr || p->right == nullptr) { // и справа нет записей
					break; // выходим
				}
				else // иначе переходим на следующую страницу
				{
					numberPage++; // увеличиваем номер страницы
					p = p->right; // перход к следующей записи
					firstNodePage = p; // первый элемент на текущей странице
				}
			}
			else {
				p = firstNodePage; // иначе сначала переписываем
			}

			break;

		case 72:
		case 73: //если нажаты Up или PgUp,			
			if (firstNodePage == nullptr || p == nullptr) {
				break;
			}
			else {
				if (firstNodePage->left == nullptr) { // если слева нет элементов
					p = firstNodePage; // р на первый эелмент и перерисовываем текущую страницу
				}
				else { //иначе
					p = firstNodePage->left; // р равняется последнему элементу на предыдущей странице
					for (int i = 1; i != countPluntRow; i++) { // доходим циклом до первого элемента на странице
						p = p->left; // перход к предыдущему элементу
					}
					numberPage--; // уменьшаем номер страницы
					firstNodePage = p; // запоминаем первый элемент на страницы
				}

			}

			break;


		default:
			p = firstNodePage; // р равняется первому элемент на текущей странице, значит перерисовыем текущую страницу
			break;
		}

	}
}

//-------------------------------------------Получаем пункт меню-------------------------------------
MainMenu getMenuItem(const std::string** menuItem, int numberMenu)
{
	// Суть функции в том, что по имени возращаем нужный элемент множества меню
	// Это нужно для switch case в главном меню
	// Функция Equal Возращает равенство строк
	// ТО бишь,е если просмотр равняется выбранному номеру меню, то мы возрашаем его элемент в множестве

	if (MAIN_MENU_ITEM[numberMenu]->_Equal("Просмотр"))
		return MainMenu::View;

	if (MAIN_MENU_ITEM[numberMenu]->_Equal("Добавить"))
		return MainMenu::Add;

	if (MAIN_MENU_ITEM[numberMenu]->_Equal("Удалить"))
		return MainMenu::Del;

	if (MAIN_MENU_ITEM[numberMenu]->_Equal("Исправить"))
		return MainMenu::Edit;

	if (MAIN_MENU_ITEM[numberMenu]->_Equal("Сортировать"))
		return MainMenu::Sort;

	if (MAIN_MENU_ITEM[numberMenu]->_Equal("Найти"))
		return MainMenu::Find;

	if (MAIN_MENU_ITEM[numberMenu]->_Equal("Сохранить"))
		return MainMenu::Save;

	if (MAIN_MENU_ITEM[numberMenu]->_Equal("Ведомость"))
		return MainMenu::Task;

	if (MAIN_MENU_ITEM[numberMenu]->_Equal("Выход"))
		return MainMenu::Exit;
}

//-------------------------------------------Вывод сообщения ошибки-------------------------------------
void error(const std::string message) {
	system("cls"); // очищаем консоль
	SetColor(ConsoleColor::Red, ConsoleColor::Yellow); // утсановка цвета и фона

	gotoxy(COUNT_SYMBOL_ROWS / 2 - 18 / 2, 9); // центрируем надпись
	printf("Произошла ошибка:");
	gotoxy(COUNT_SYMBOL_ROWS / 2 - strlen(message.c_str()) / 2, 10); // центрируем надпись
	// как работают готу выше
	// например у нас длина  строки 120 символов.
	// длина выводим строки 18 символов
	// сначала находим середина 120/2=60
	// находим середину нужной надписи 18/2=9
	// из середины строки вычитаем серидину надписи и в итоге получаем текст ровно по середине, то бишь 60-9=51 и текст будет с 51 по 69 символ
	printf(message.c_str()); // печатаем надпись
	_getch(); // ждём нажатия любой клавиши

	SetColor(ConsoleColor::Black, ConsoleColor::White); // Возвращаем цвет
	system("cls"); // очищаем консоль
}

// -----Установить курсор в позицию--------------------------------
void gotoxy(int x, int y) {
	COORD pos = { x, y }; // Объект позицию на экране
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE); // получаем хэндл на поток вывода
	SetConsoleCursorPosition(output, pos); // устанавливаем курсор в нужное место
}

//-----------------------------------Добавление элемента в конец списка-----------------------------
void add(elist*& beg, elist*& fin, plunt newPlunt) {
	elist* p; //указатель на создаваемый элемент
	p = new elist; //создание нового элемента
	p->data = newPlunt; //заполнение его информационной части
	p->right = nullptr; //заполнение его указателей
	p->left = nullptr; // слева нал
	if (beg == nullptr) { // если список пуст
		beg = fin = p; //устанавливаем элемент на начало и конец списка
	}
	else // иначе добавлаем в конпец
	{
		p->left = fin; // слева от нового элемента указываем на последний элемент списка
		fin->right = p; // справа от последнего элемента списка, новый элемент
		fin = p; // перемещаем конец списка на новый элемент
	}

}

//--------------------------------------------Ввод базы из файла---------------------------------------------
int readfile(elist*& beg, elist*& fin, char* fileName) {
	FILE* fp; // указатель на файл
	std::string* name = new std::string(NAME_FILE_DIRECTORY); // получаем имя директории
	name->append(fileName, strlen(fileName) - 1); // соединяем с введённым именем пользователя

	plunt newPlunt;

	fopen_s(&fp, name->c_str(), "r"); // открываем файл для чтения
	if (fp == NULL) { // файла не существует
		// собираем надпись, напримр "Файл test.txt не найден"
		std::string* errorStr = new std::string("Файл ");
		errorStr->append(fileName, strlen(fileName) - 1);
		errorStr->append(" не найден");

		error(errorStr->c_str()); //выводим ошибку
		return 1; // выходим из функции
	}

	while (!feof(fp)) { // пока не наступит конец файоа
		fscanf_s(fp, "%d\n", &newPlunt.pluntNumber); // Номер завода
		fscanf_s(fp, "%d\n", &newPlunt.filialNumber); //Номер филиала
		fgets(newPlunt.surname, SURNAME_LENGTH, fp); //чтение фамилии
		fscanf_s(fp, "%lf\n", &newPlunt.startPeriodMoney);
		fscanf_s(fp, "%lf\n", &newPlunt.receivedMoney);
		if (fscanf_s(fp, "%lf\n", &newPlunt.droppedMoney) == 1)// если у нас что-то считалось, значит запись и файл верного формата
			add(beg, fin, newPlunt); //добавление в список
	}

	fseek(fp, 0, SEEK_END); // Очистка буфера файла

	fclose(fp);

	return 0;
}

//--------------------------------------сохранение в файл-------------------------
void saveFile(char* fileName, elist*& beg) {
	// тут всё по аналогии с чтением файла
	FILE* fp;
	std::string* name = new std::string(NAME_FILE_DIRECTORY);
	name->append(fileName, strlen(fileName) - 1);

	plunt newPlunt;

	fopen_s(&fp, name->c_str(), "w"); // открываем файл на запись, если его не существует он создаёт
	if (fp == NULL) {
		std::string* errorStr = new std::string("Файл ");
		errorStr->append(fileName, strlen(fileName) - 1);
		errorStr->append(" не найден");

		error(errorStr->c_str());
		return;
	}

	elist* p = beg;
	// выводим весь списко в файл
	while (p != nullptr) {
		fprintf(fp, "%d\n", p->data.pluntNumber);
		fprintf(fp, "%d\n", p->data.filialNumber);
		fprintf(fp, "%s", p->data.surname);
		fprintf(fp, "%lf\n", p->data.startPeriodMoney);
		fprintf(fp, "%lf\n", p->data.receivedMoney);
		fprintf(fp, "%lf\n", p->data.droppedMoney);

		p = p->right;
	}

	fclose(fp); // закрываем файл
}

//--------------------------------------Выполнение задания-------------------------
void calculationTask(elist*& beg, elist*& fin) {
	// здесь аналогия с сохранение файоа
	char* fileName = new char[FILE_NAME_LENGTH];
	printf("Выполнения \n");
	printf("Введите имя файла для сохранения: ");
	fseek(stdin, 0, SEEK_END); // Очистка буфера scanf

	SetConsoleCP(1251);
	fgets(fileName, FILE_NAME_LENGTH, stdin);
	SetConsoleCP(866);

	FILE* fp;
	std::string* name = new std::string(NAME_FILE_DIRECTORY);
	name->append(fileName, strlen(fileName) - 1);

	plunt newPlunt;

	fopen_s(&fp, name->c_str(), "w"); // открывает или создаём или перезаписываем файл на чтение
	if (fp == NULL) {
		std::string* errorStr = new std::string("Файл ");
		errorStr->append(fileName, strlen(fileName) - 1);
		errorStr->append(" не найден");

		error(errorStr->c_str());
		return;
	}

	elist* beginPlunt, * finishPlunt;
	clear(); // очищаем
	gotoxy(0, 2);
	printf("%s\t%s: %s", "Статистика", "Сохраняем в файл", fileName); // пишем заголовочную информацию

	beginPlunt = finishPlunt = nullptr; // начало и конец списка по заводам

	double start, recived, dropped; // общее количество данных
	start = recived = dropped = 0; // обнуляем

	elist* p = beg;
	while (p != nullptr) {// пока не наступит конец списка
		/*
		добавление я уже описал много раз выше, тут пропущу
		*/
		if (beginPlunt == nullptr) { // если список пустой, то добавляем в начало
			beginPlunt = finishPlunt = new elist();
			beginPlunt->data = p->data;
		}
		else {
			elist* q = beginPlunt;
			while (q != nullptr && q->data.pluntNumber != p->data.pluntNumber) { // ищем номер завода
				q = q->right;
			}

			if (q == nullptr) { // если такого завода нет, то мы его добавляем
				finishPlunt->right = new elist();
				finishPlunt->right->left = finishPlunt;
				finishPlunt = finishPlunt->right;
				finishPlunt->right = nullptr;
				finishPlunt->data = p->data;
			}
			else { // если такой завод уже есть в списке заводов, то мы увеличиваем его значения, то есть считаем общее значение
				q->data.startPeriodMoney += p->data.startPeriodMoney;
				q->data.receivedMoney += p->data.receivedMoney;
				q->data.droppedMoney += p->data.droppedMoney;
			}
		}

		start += p->data.startPeriodMoney;
		recived += p->data.receivedMoney;
		dropped += p->data.droppedMoney;

		p = p->right;
	}

	clear();
	gotoxy(0, 2);
	/*
	дальше пойдёт вывод в консоль и вывод на экран, про спецсимволы описано выше. Всё логично и интуитивно понятно
	*/

	// выводим все филиалы
	printf("%s\t%s\t\t%s\t\t%s\t\t\t%s\t\t\n", "Номер филиала", "В начале", "Получено", "Убыло", "Итого");
	fprintf_s(fp, "%s\t%s\t\t%s\t\t%s\t\t\t%s\t\t\n", "Номер филиала", "В начале", "Получено", "Убыло", "Итого");
	p = beg;
	while (p != nullptr) {
		printf("%d\t\t%lf\t\t%lf\t\t%lf\t\t%lf\t\t\n",
			p->data.filialNumber,
			p->data.startPeriodMoney,
			p->data.receivedMoney,
			p->data.droppedMoney,
			p->data.startPeriodMoney + p->data.receivedMoney - p->data.droppedMoney);
		fprintf_s(fp, "%d\t\t%lf\t\t%lf\t\t%lf\t\t%lf\t\t\n",
			p->data.filialNumber,
			p->data.startPeriodMoney,
			p->data.receivedMoney,
			p->data.droppedMoney,
			p->data.startPeriodMoney + p->data.receivedMoney - p->data.droppedMoney);

		p = p->right;
	}


	// вставляем две пустых строки
	printf("\n\n");
	fprintf_s(fp, "\n\n");

	p = beginPlunt;
	printf("%s\t%s\t\t%s\t\t%s\t\t\t%s\t\t\n", "Номер завода", "В начале", "Получено", "Убыло", "Итого");
	fprintf_s(fp, "%s\t%s\t\t%s\t\t%s\t\t\t%s\t\t\n", "Номер завода", "В начале", "Получено", "Убыло", "Итого");
	// выводим все заводы
	while (p != nullptr) {
		printf("%d\t\t%lf\t\t%lf\t\t%lf\t\t%lf\t\t\n",
			p->data.pluntNumber,
			p->data.startPeriodMoney,
			p->data.receivedMoney,
			p->data.droppedMoney,
			p->data.startPeriodMoney + p->data.receivedMoney - p->data.droppedMoney);

		fprintf_s(fp, "%d\t\t%lf\t\t%lf\t\t%lf\t\t%lf\t\t\n",
			p->data.pluntNumber,
			p->data.startPeriodMoney,
			p->data.receivedMoney,
			p->data.droppedMoney,
			p->data.startPeriodMoney + p->data.receivedMoney - p->data.droppedMoney);

		p = p->right;
	}

	// вставляем две пустых строки
	printf("\n\n");
	fprintf_s(fp, "\n\n");
	// выыводим общие данные
	// чтобы получить итог мы к начальным данным прибавляем полученные и вычитаем ушедшие.

	fprintf_s(fp, "Итого\nВсего в начале: %lf\nВсего получено: %lf\nВсего убыло: %lf\nОстаток: %lf\n",
		start, recived, dropped, start + recived - dropped);

	printf("Итого\nВсего в начале: %lf\nВсего получено: %lf\nВсего убыло: %lf\nОстаток: %lf\n",
		start, recived, dropped, start + recived - dropped);

	fclose(fp); // закрываем файл
}


